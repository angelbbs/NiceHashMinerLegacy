# Miner Legacy Fork Fix (for NiceHash)
## This is unofficial version of NHML

Miner Legacy Fork Fix (for NiceHash) is a fork of NiceHash Miner Legacy. This version is intended for experienced miners who want the quickest updates and highest hashrates!
The development has been underway since 2017.

[![GitHub release](https://img.shields.io/github/release/angelbbs/NiceHashMinerLegacy/all.svg)](https://github.com/angelbbs/NiceHashMinerLegacy/releases)
[![GitHub Release Date](https://img.shields.io/github/release-date/angelbbs/NiceHashMinerLegacy.svg)](https://github.com/angelbbs/NiceHashMinerLegacy/releases)

[![Github All Releases](https://img.shields.io/github/downloads/angelbbs/NiceHashMinerLegacy/total.svg)](https://github.com/angelbbs/NiceHashMinerLegacy/releases)
## Download
* **[Download latest release](https://github.com/angelbbs/NiceHashMinerLegacy/releases)**


"Miner Legacy Fork Fix" is an free open source easy to use CPU & GPU cryptocurrency miner program with classic GUI for Windows.
- Easy installer.
- Support AMD, INTEL, NVIDIA GPUs and CPU.
- Included most profitable miners programs.
- Dual algorithms.
- Fast benchmark - 1-2 min per algo.
- Auto-switch between most profitable algorithms taking into account the consumption of devices and the cost of electricity.
- Tariff zones for electricity.
- GPU overclocking via MSI Afterburner.
- Support overclocking profiles, benchmarks, miners additional parameters.
- Program automatic update and backup.
- Colors profiles.
- Built-in watchdog.
- Remote desktop viewer via browser;
- REST API.
- ......and many other options and features.
