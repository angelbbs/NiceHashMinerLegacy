﻿namespace NiceHashMiner.Stats.V4
{
    public enum NhmwsSetResult
    {
        CHANGED = 0,
        NOTHING_TO_CHANGE,
        INVALID
    }
}
