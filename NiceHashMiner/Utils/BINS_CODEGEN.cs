namespace NiceHashMiner.Utils
{
    public static class MinersBins
    {
        public static string[] ALL_FILES_BINS =
        {
            @"/vc_redist.x64.exe",
            @"/xmrig/xmrig.exe",
            @"/Nanominer/nanominer.exe",
            @"/phoenix/PhoenixMiner.exe",
            @"/SRBMiner/SRBMiner-MULTI.exe",
            @"/teamredminer/teamredminer.exe",
            @"/gminer/miner.exe",
            @"/lolMiner/lolMiner.exe",
            @"/miniZ/miniZ.exe",
            @"/Rigel/rigel.exe",
            @"/Fork_Fix_68.txt"
        };
    }
}
